from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
import logging
db_logger = logging.getLogger('db')

db_logger.info('info message')
db_logger.warning('warning message')


def test_log(request):
    try:
        1/0
    except Exception as e:
        db_logger.exception(e)
        return HttpResponse(e)